$(document).mouseup(function (e) {
    let container = $(".formdiv");
    if (container.has(e.target).length === 0) {
        container.hide();
        $(".formdiv").css("display", "none");
        history.pushState(false, "", ".");
    }
});

function loadStorage() {
    if (localStorage.getItem("name") !== null)
        $("#name").val(localStorage.getItem("name"));
    if (localStorage.getItem("email") !== null)
        $("#email").val(localStorage.getItem("email"));
    if (localStorage.getItem("text") !== null)
        $("#text").val(localStorage.getItem("text"));
    if (localStorage.getItem("agree") !== null) {
        $("#agree").prop("checked", localStorage.getItem("agree") === "true");
        if ($("#agree").prop("checked"))
            $("#send").removeAttr("disabled");
    }
}
function saveStorage() {
    localStorage.setItem("name", $("#name").val());
    localStorage.setItem("email", $("#email").val());
    localStorage.setItem("text", $("#text").val());
    localStorage.setItem("agree", $("#agree").prop("checked"));
}

function clear() {
    localStorage.clear();
    $("#name").val("");
    $("#email").val("");
    $("#text").val("");
    $("#agree").val(false);
}
function openButton() {
    $(".formdiv").css("display", "flex");
    history.pushState(true, "", "./form");
}

function closeButton() {
    $(".formdiv").css("display", "none");
    history.pushState(false, "", ".");
}
function agreeButton() {
    if(this.checked)
        $("#send").removeAttr("disabled");
    else
        $("#send").attr("disabled", "");
}
$(document).ready(function() {
    loadStorage();
    $("#open").click(openButton);
    $("#close").click(closeButton);
    $("#form").submit(function(check) {
        check.preventDefault();
        $(".formdiv").css("display", "none");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://formcarry.com/s/LAv_ftLrAI",
            data: $(this).serialize(),
            success: function(response){
                if(response.status == "success"){
                    alert("success");
                    clear();
                } else {
                    alert("not success" + response.message);
                }
            }
        });
    });
    $("#agree").change(agreeButton);
    $("#form").change(saveStorage);
    window.onpopstate = function(event) {
        if (event.state)
            $(".formdiv").css("display", "flex");
        else
            $(".formdiv").css("display", "none");
    };
})

